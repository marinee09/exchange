<?php
namespace App\Http\Controllers;

use App\Email;
use App\Http\Requests\EmailRequest;

class EmailController extends Controller
{

    public function getForm()
    {
        return view('email');
    }

    public function postForm(EmailRequest $request)
    {
        // Ici on crée une nouvelle instance deEmail
        $email = new Email;
        //  On affecte l'attribut email avec la valeur de l'entrée
        $email->email = $request->input('email');
        // Enfin on demande au modèle d'enregistrer cette ligne effectivement dans la table (save).
        $email->save();

        return view('email_ok');
    }

}